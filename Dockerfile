FROM docker.io/library/openjdk:8-jdk-alpine

WORKDIR /app

COPY complete/target/*.jar .

EXPOSE 8080

ENTRYPOINT ["java","-jar","/app/*.jar"]
